---------------------------
----------------------------
Before running the program:
----------------------------
----------------------------

Hook up webcam to android device emulator:
------------------------------------------
1. Tools > Android > AVD Manager
2. Under Actions, select Edit for your virtual device
3. Show Advanced Settings
4. Camera Front: Webcam0
   Camera Back:  Webcam0

Install OpenCV Manager on android device emulator:
--------------------------------------------------
1. Open Terminal in Android Studio
2. E:\AndroidStudio\p05-amaya\Stroke\platform-tools\adb.exe install E:\AndroidStudio\p05-amaya\Stroke\apk\OpenCV_3.2.0_Manager_3.20_x86.apk
   This was specific to the architecture of my virtual device. I used the Nexus 5x. If your device is different, look in the above apk folder for the correct apk architecture version.

------------------------
------------------------
Running the mobile app:
------------------------
------------------------
The idea for this app came from the Cincinnati Stroke Scale:
1) Arm drift
2) Slurred speech
3) Facial drooping
Studies had shown that people were able to detect arm drift and slurred speech with greater accuracy than facial drooping, so the idea was for the app to detect facial asymmetry as a risk of stroke. I was not able to complete this portion to my liking as it consumed too many resources, so instead, I implemented a grid to give the app user a physical aid in facial asymmetry. After opening the app, press the start button to begin. A camera view will pop up, with facial detection. Align the face as centered as possible to the crosshairs in the square. Tap the screen to freeze the frame. You should see a more detailed grid. Carefully look for differences in facial symmetry. Please note that the touch detection is buggy on the emulator, so you may need to try several times. 
