package vamaya1.stroke;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton buttonStartCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // portrait orientation
        buttonStartCamera = (ImageButton)findViewById(R.id.buttonStartCamera); // get start button
        buttonStartCamera.setOnClickListener(this); // add click listener
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, CameraActivity.class)); // start camera activity
    }
}
