package vamaya1.stroke;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;
import org.opencv.android.*;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class CameraActivity extends Activity
        implements CvCameraViewListener2 {

    private boolean snapped;
    Mat snapshot;
    Point BR;
    Point TL;

    private CameraBridgeViewBase openCvCameraView;
    private CascadeClassifier cascadeClassifier;
    private Mat grayscaleImage;
    private int absoluteFaceSize;

    // These variables are used (at the moment) to fix camera orientation from 270degree to 0degree
    Mat mRgba;
    Mat mRgbaF;
    Mat mRgbaT;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    initializeOpenCVDependencies();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
                }
            }
    };

    private void initializeOpenCVDependencies() {

        try {
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();
            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

        // And we are ready to go
        openCvCameraView.enableView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //snapPicture = false;

        openCvCameraView = new JavaCameraView(this, -1);
        setContentView(openCvCameraView);
        openCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        grayscaleImage = new Mat(height, width, CvType.CV_8UC4);

        // The faces will be a 20% of the height of the screen
        absoluteFaceSize = (int) (height * 0.2);

        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRgbaF = new Mat(height, width, CvType.CV_8UC4);
        mRgbaT = new Mat(width, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        if (snapped) {

            double dx = BR.x - TL.x;
            double dy = BR.y - TL.y;
            // quarter grids
            double xQtr1 = TL.x + (dx / 4.0);
            double yQtr1 = TL.y + (dy / 4.0);
            double xQtr2 = BR.x - (dx / 4.0);
            double yQtr2 = BR.y - (dy / 4.0);
            Imgproc.line(grayscaleImage, new Point(xQtr1, BR.y), new Point(xQtr1, TL.y), new Scalar(0, 255, 0, 255), 2);
            Imgproc.line(grayscaleImage, new Point(BR.x, yQtr1), new Point(TL.x, yQtr1), new Scalar(0, 255, 0, 255), 2);
            Imgproc.line(grayscaleImage, new Point(xQtr2, BR.y), new Point(xQtr2, TL.y), new Scalar(0, 255, 0, 255), 2);
            Imgproc.line(grayscaleImage, new Point(BR.x, yQtr2), new Point(TL.x, yQtr2), new Scalar(0, 255, 0, 255), 2);
            // eights
            double x8_1 = TL.x + (dx / 8.0);
            double y8_1= TL.y + (dy / 8.0);
            double x8_2 = xQtr1 + (dx / 8.0);
            double y8_2 = yQtr1 + (dy / 8.0);
            double x8_3 = xQtr2 - (dx / 8.0);
            double y8_3 = yQtr2 - (dy / 8.0);
            double x8_4 = BR.x - (dx / 8.0);
            double y8_4 = BR.y - (dy / 8.0);
            Imgproc.line(grayscaleImage, new Point(x8_1, BR.y), new Point(x8_1, TL.y), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(BR.x, y8_1), new Point(TL.x, y8_1), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(x8_2, BR.y), new Point(x8_2, TL.y), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(BR.x, y8_2), new Point(TL.x, y8_2), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(x8_3, BR.y), new Point(x8_3, TL.y), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(BR.x, y8_3), new Point(TL.x, y8_3), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(x8_4, BR.y), new Point(x8_4, TL.y), new Scalar(0, 255, 0, 255), 1);
            Imgproc.line(grayscaleImage, new Point(BR.x, y8_4), new Point(TL.x, y8_4), new Scalar(0, 255, 0, 255), 1);

            return snapshot;
        }
        else {
            grayscaleImage = inputFrame.gray(); // create gray scale image
            MatOfRect faces = new MatOfRect(); // create array for faces
            Core.flip(grayscaleImage, mRgbaF, 0); // adjust image to correct orientation
            grayscaleImage = mRgbaF;
            // Use the classifier to detect faces
            if (cascadeClassifier != null) {
                cascadeClassifier.detectMultiScale(grayscaleImage, faces, 1.1, 2, 2,
                        new Size(absoluteFaceSize, absoluteFaceSize), new Size());
            }
            // draw rectangle around face found
            Rect[] facesArray = faces.toArray();
            if (facesArray.length == 1) {
                Imgproc.rectangle(grayscaleImage, facesArray[0].tl(), facesArray[0].br(), new Scalar(0, 255, 0, 255), 3);
                // draw cross hairs
                double dx = facesArray[0].br().x - facesArray[0].tl().x;
                double dy = facesArray[0].br().y - facesArray[0].tl().y;
                double xMid = facesArray[0].tl().x + (dx / 2.0);
                double yMid = facesArray[0].tl().y + (dy / 2.0);
                // half grids
                Imgproc.line(grayscaleImage, new Point(xMid, facesArray[0].br().y), new Point(xMid, facesArray[0].tl().y), new Scalar(0, 196, 0, 255), 2);
                Imgproc.line(grayscaleImage, new Point(facesArray[0].br().x, yMid), new Point(facesArray[0].tl().x, yMid), new Scalar(0, 196, 0, 255), 2);
                TL = facesArray[0].tl();
                BR = facesArray[0].br();
                snapshot = grayscaleImage;
            }
            return grayscaleImage;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (snapped) {
            snapped = false;
        }
        else {
            snapped = true;
        }
        Log.d("clicked screen!", "turned " + snapped);
        return super.onTouchEvent(event);
    }
}